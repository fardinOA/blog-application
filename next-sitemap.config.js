module.exports = {
    siteUrl: "https://lyceum-article.vercel.app", // Production
    exclude: ["/404"],
    generateRobotsTxt: true,
    robotsTxtOptions: {
        policies: [
            {
                userAgent: "*",
                disallow: ["/404"],
            },
            { userAgent: "*", allow: "/" },
        ],
        additionalSitemaps: [
            `https://lyceum-article.vercel.app/sitemap.xml`,
            `https://lyceum-article.vercel.app/server-sitemap.xml`,
        ],
    },
};
