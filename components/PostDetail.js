import React, { Fragment } from "react";
import moment from "moment";
import Head from "next/head";
import Image from "next/legacy/image";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { motion } from "framer-motion";
const PostDetail = ({ post }) => {
    console.log(post);
    const getContentFragment = (index, text, obj, type) => {
        let modifiedText = text;
        if (obj) {
            if (obj.bold) {
                modifiedText = <b key={index}>{text}</b>;
            }
            if (obj.italic) {
                modifiedText = <em key={index}>{text}</em>;
            }
            if (obj.underline) {
                modifiedText = <u key={index}>{text}</u>;
            }
        }

        switch (type) {
            case "numbered-list":
                return (
                    <ol key={index} className=" text-xl font-semibold my-4">
                        {modifiedText.map((item, i) => (
                            <li key={i}>{item}</li>
                        ))}
                    </ol>
                );
            case "block-quote":
                return (
                    <q key={index} className=" text-xl font-semibold my-4">
                        {modifiedText.map((item, i) => (
                            <Fragment key={i}>{item}</Fragment>
                        ))}
                    </q>
                );
            case "heading-three":
                return (
                    <h3 key={index} className=" text-xl font-semibold my-4">
                        {modifiedText.map((item, i) => (
                            <Fragment key={i}>{item}</Fragment>
                        ))}
                    </h3>
                );
            case "paragraph":
                return (
                    <p key={index} className=" mb-8">
                        {modifiedText?.map((item, i) => (
                            <Fragment key={i}>{item}</Fragment>
                        ))}
                    </p>
                );
            case "heading-four":
                return (
                    <h4 key={index} className=" text-md font-semibold mb-4">
                        {modifiedText.map((item, i) => (
                            <Fragment key={i}>{item}</Fragment>
                        ))}
                    </h4>
                );
            case "image":
                return (
                    <img
                        key={index}
                        alt={obj.title}
                        height={obj.height}
                        width={obj.width}
                        src={obj.src}
                    />
                );
            default:
                return modifiedText;
        }
    };

    return (
        <motion.div className=" overflow-x-hidden  bg-black bg-opacity-20 text-white  rounded-lg lg:p-8 pb-12 mb-8">
            <Head>
                <title>{post?.title}</title>
                <meta
                    name="description"
                    content="Best blog site on the planet for MERN stack developer. Here you find up to date content about javascript technology."
                />
                <meta name="keyword" content={`${post?.title}  `} />
            </Head>
            <div className="relative overflow-hidden   mb-6">
                <Image
                    height={100}
                    width={100}
                    loading="lazy"
                    src={post?.featuredImage.url}
                    alt={post?.title}
                    layout="responsive"
                    className="object-top h-full w-full rounded-t-lg"
                />
            </div>
            <div className="px-4 lg:px-0 ">
                <div className="flex items-center justify-between mb-8 w-full">
                    <div className="flex item-center mb-4 mt-7 mr-8 lg:mb-0 lg:w-auto">
                        <Image
                            src={post?.author.photo.url}
                            alt={post?.author.name}
                            height={30}
                            width={30}
                            loading="lazy"
                            className="align-middle rounded-full "
                        />
                        <p className="inline align-middle font-Inter_Regular text-white ml-2 text-lg">
                            {post?.author.name}
                        </p>
                    </div>
                    <div className="text-blue-400 font-midume flex flex-row  justify-center">
                        <svg
                            className="w-6 h-7 mr-2"
                            fill="none"
                            stroke="currentColor"
                            viewBox="0 0 24 24"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                            ></path>
                        </svg>
                        <span className="mt-1">
                            {moment(post?.createdAt).format("MMM DD,YYYY")}
                        </span>
                    </div>
                </div>
                <h1 className="text-3xl text-white font-semibold mb-6">
                    {post?.title}
                </h1>
                {post?.content.raw.children.map((typeObj, ind) => {
                    if (typeObj.type == "code-block") {
                        return typeObj.children.map((ele, ind) => {
                            return (
                                <SyntaxHighlighter
                                    key={ind}
                                    language="javascript"
                                >
                                    {ele.text}
                                </SyntaxHighlighter>
                            );
                        });
                    }

                    if (typeObj.type == "class") {
                        return typeObj.children.map((ele, ind) => {
                            return ele.children.map((ele2, ind2) => {
                                return (
                                    <div
                                        key={ind2}
                                        className="my-4 text-white relative max-h-max w-full p-6  border-[1px] border-red-500"
                                    >
                                        <span className="mt-8   ">
                                            {" "}
                                            {ele2.text}
                                        </span>
                                        <span className="truncate absolute top-0 right-0 px-2 rounded-bl-md bg-red-500">
                                            {typeObj.className}
                                        </span>{" "}
                                    </div>
                                );
                            });
                        });
                    }
                    const children = typeObj.children.map((item, itemInd) =>
                        getContentFragment(itemInd, item.text, item)
                    );
                    let nn = {
                        type: typeObj.type,
                        ...children,
                    };

                    if (typeObj.type == "bulleted-list") {
                        return (
                            <ul className="list-disc list-outside ml-4 mb-8   rounded-lg p-2 text-[1.3rem]  ">
                                {typeObj.children.map((ele, ind) => {
                                    return ele.children.map((cEle, cInd) => {
                                        return cEle.children.map(
                                            (item, itemInd) => {
                                                return (
                                                    <li
                                                        className="my-2"
                                                        key={itemInd}
                                                    >
                                                        {item.text}
                                                    </li>
                                                );
                                            }
                                        );
                                    });
                                })}
                            </ul>
                        );
                    }
                    if (typeObj.type == "numbered-list") {
                        return (
                            <ul className="list-decimal list-outside ml-4 mb-8   rounded-lg p-2 text-[1.3rem]">
                                {typeObj.children.map((ele, ind) => {
                                    return ele.children.map((cEle, cInd) => {
                                        return cEle.children.map(
                                            (item, itemInd) => {
                                                return (
                                                    <li
                                                        className="my-2"
                                                        key={itemInd}
                                                    >
                                                        {item.text}
                                                    </li>
                                                );
                                            }
                                        );
                                    });
                                })}
                            </ul>
                        );
                    }
                    if (typeObj.type == "code-block") {
                        return (
                            <div>
                                {
                                    'int main(){\n cout<<"this is a test code"<<endl;\n}\n'
                                }
                            </div>
                        );
                    }
                    if (typeObj.type == "table") {
                        return (
                            <table className="table border-collapse w-full border border-grey-light">
                                {typeObj.children.map((e1, i1) => {
                                    return (
                                        <tbody key={i1}>
                                            {e1.children.map((e2, i2) => {
                                                return (
                                                    <tr key={i2}>
                                                        {e2.children.map(
                                                            (e3, i3) => {
                                                                return e3.children.map(
                                                                    (
                                                                        e4,
                                                                        i4
                                                                    ) => {
                                                                        return e4.children.map(
                                                                            (
                                                                                e5,
                                                                                i5
                                                                            ) => {
                                                                                return (
                                                                                    <td
                                                                                        key={
                                                                                            i5
                                                                                        }
                                                                                        className={`${
                                                                                            e1.type ==
                                                                                                "table_head" &&
                                                                                            "bg-gray-800   font-black"
                                                                                        } border  border-grey-dark item capitalize    p-auto text-center   text-base font-bold  `}
                                                                                    >
                                                                                        {
                                                                                            e5.text
                                                                                        }
                                                                                    </td>
                                                                                );
                                                                            }
                                                                        );
                                                                    }
                                                                );
                                                            }
                                                        )}
                                                    </tr>
                                                );
                                            })}
                                        </tbody>
                                    );
                                })}
                            </table>
                        );
                    }
                    // let children = typeObj.children;
                    // console.log("children", typeObj);

                    return getContentFragment(
                        ind,
                        children,
                        typeObj,
                        typeObj.type
                    );
                    // return null;
                })}
            </div>
        </motion.div>
    );
};

export default PostDetail;
