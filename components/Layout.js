import Script from "next/script";
import React from "react";
import Header from "./Header";

const Layout = ({ children }) => {
    return (
        <React.Fragment>
            {/* google add */}
            {/* <Script
                async
                src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8266021959700543"
                crossorigin="anonymous"
            ></Script> */}
            {/* google analytics */}
            {/* <Script
                strategy={"afterInteractive"}
                src={`https://www.googletagmanager.com/gtag/js?id=G-5QSF7D1497`}
            /> */}
            {/* <Script
                id={"google-analytics"}
                strategy={"afterInteractive"}
                dangerouslySetInnerHTML={{
                    __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
        
            gtag('config', 'G-5QSF7D1497');
        `,
                }}
            /> */}
            <Header />
            {children}
        </React.Fragment>
    );
};

export default Layout;
