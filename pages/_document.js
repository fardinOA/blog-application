import { DefaultSeo } from "next-seo";
import Document, { Html, Head, Main, NextScript } from "next/document";
import SEO from "../nex-seo.config";
class MyDocument extends Document {
    render() {
        return (
            <Html lang="en">
                <Head>
                    <link rel="manifest" href="/manifest.json" />
                    <link
                        rel="apple-touch-icon"
                        href="/lycemu-logo/default.png"
                    ></link>
                    <meta name="theme-color" content="#fff" />
                    <meta name="robots" content="all" />
                </Head>

                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
