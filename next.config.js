const withPWA = require("next-pwa")({
    dest: "public",
    disable: process.env.NODE_ENV === "development",

    register: true,
    scope: "/app",
    sw: "service-worker.js",
});
module.exports = withPWA({
    // The starter code load resources from `public` folder with `router.basePath` in React components.
    // So, the source code is "basePath-ready".
    // You can remove `basePath` if you don't need it.
    reactStrictMode: true,

    images: {
        domains: ["media.graphassets.com", "*"],
    },
    i18n: {
        locales: ["en", "bd"],
        defaultLocale: "en",
    },
});
// const withPWA = require("next-pwa");

// module.exports = withPWA({
//     pwa: {
//         dest: "public",
//         register: true,
//         skipWaiting: true,
//     },

//     images: {
//         domains: ["media.graphassets.com", "*"],
//     },
// });
