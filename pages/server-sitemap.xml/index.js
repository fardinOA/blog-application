import { getServerSideSitemap } from "next-sitemap";
import { getPosts } from "../../services";

export const getServerSideProps = async (ctx) => {
    const posts = (await getPosts()) || [];
    posts = await posts.json();
    const newsSitemaps = posts.map((item) => ({
        loc: `${process.env.NEXT_PUBLIC_DOMAIN_URL}${item.node.slug})}`,
        lastmod: new Date().toISOString(),
    }));

    const fields = [...newsSitemaps];

    return getServerSideSitemap(ctx, fields);
};

export default function Site() {}
