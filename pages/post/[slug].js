import React from "react";
import { getPosts, getPostDetails } from "../../services";
import { useRouter } from "next/router";
import {
    PostDetail,
    Categories,
    PostWidget,
    Author,
    Comments,
    CommentFrom,
    Loader,
} from "../../components";
import Head from "next/head";
const PostDetails = ({ post }) => {
    const router = useRouter();
    if (router.isFallback) {
        <Loader />;
    }

    return (
        <>
            {post ? (
                <>
                    <Head>
                        <title>{post?.slug}</title>
                        <link rel="icon" href="/icons/icon-48x48.png" />
                        <link rel="manifest" href="/manifest.json" />
                    </Head>
                    <div
                        data-scroll-section
                        className="container mx-auto px-4 md:px-10 mb-8"
                    >
                        <div className="grid grid-cols-1 lg:grid-cols-12 gap-12">
                            <div className="col-span-1 lg:col-span-8">
                                <PostDetail post={post} />
                                <Author author={post?.author} />
                                <CommentFrom slug={post?.slug} />
                                <Comments slug={post?.slug} />
                            </div>
                            <div className="col-span-1 lg:col-span-4">
                                <div className="relative lg:sticky top-8">
                                    <PostWidget
                                        slug={post?.slug}
                                        categories={post?.categories?.map(
                                            (ele) => ele.slug
                                        )}
                                    />
                                    <Categories />
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <div className="px-10">
                    <div className=" mb-20   flex justify-center items-center">
                        {" "}
                        <p className="bg-white text-center animate-bounce capitalize p-4 rounded-md ">
                            There Is No post with this slug
                        </p>
                    </div>{" "}
                    <p className=" text-white bg-black mb-10 font-Inter_Black text-[1rem] text-center capitalize p-4 rounded-md">
                        Please Select your post categories here
                    </p>
                    <Categories />
                </div>
            )}
        </>
    );
};

export default PostDetails;

export const getStaticProps = async ({ params }) => {
    const data = await getPostDetails(params.slug);

    return {
        props: {
            post: data,
        },
        revalidate: 10,
    };
};

export const getStaticPaths = async () => {
    const res = await getPosts();

    const paths = res.map((post) => ({
        params: {
            slug: `${post.node.slug}`,
        },
    }));

    return {
        paths,
        fallback: true,
    };
};
