import Head from "next/head";
import Categories from "../components/Categories";
import PostCard from "../components/PostCard";
import PostWidget from "../components/PostWidget";
import { getPosts } from "../services";
import FeaturedPost from "../sections/FeaturedPost";
import { Loader } from "../components";
import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";

export default function Home({ posts }) {
    const [showWelcomePage, setShowWelcomePage] = useState(true);
    const router = useRouter();
    const ref = useRef(null);

    useEffect(() => {
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.register("/service-worker.js");
        }
    }, []);

    if (router.isFallback) {
        <Loader />;
    }

    let allTags = "lyceum, articles, js tutorial, javascript tutorial";
    posts.map((post) => {
        allTags += `, ${post.node.slug}`;
    });

    return (
        <div
            ref={ref}
            id="top"
            className="container relative mx-auto px-6 lg:px-10 mb-8 z-0  transition-all duration-500 "
        >
            <Head>
                <link rel="icon" href="/lyceum-logo/default.png" />
                <link rel="manifest" href="/manifest.json" />

                <meta name="keyword" content={`${allTags}`} />
                <meta
                    name="ahrefs-site-verification"
                    content="6541c4408f02772fbd5ab2363a32bddc945daf0a9f91ddd14c720662731041ce"
                ></meta>
                {/*<!-- Facebook Meta Tags -->*/}
                <meta property="og:title" content="fardin omor afnan" />
                <meta
                    property="og:description"
                    content="I'm a simple person. If anybody irritated me, I became the most complicated person in the world.😈"
                />
                <meta
                    property="og:image"
                    content={`https://scontent.fdac135-1.fna.fbcdn.net/v/t39.30808-6/271695046_2751402775168253_1215555121385615814_n.jpg?stp=dst-jpg_p640x640&_nc_cat=105&ccb=1-7&_nc_sid=e3f864&_nc_eui2=AeH7t5bJjUHO19R_zU1bP3gNgxHlAZoATsGDEeUBmgBOwcPWM8rYe18yoQ6oqELmAZVZ82u7CUprldv0hXLj8Sxc&_nc_ohc=ViIBL8cQjP8AX-lRdOU&_nc_ht=scontent.fdac135-1.fna&oh=00_AfDtkmjv2wQOZg_9kolNcgrqH4jszP8IXt8Sr7GQpMpzNQ&oe=6390CB28`}
                />
                <meta
                    property="og:url"
                    content={`https://www.facebook.com/fo.afnan/`}
                />
                <meta property="og:type" content="website" />

                <meta name="author" content="Fardin Omor Afnan"></meta>
            </Head>
            <div className=" p-4 mx-10 text-[2rem]">
                <marquee behavior="" direction="">
                    Read Article & Learn
                </marquee>
            </div>

            <div>
                <FeaturedPost />
            </div>
            <div className=" grid grid-cols-1 lg:grid-cols-12 gap-12 transition-all duration-500">
                <div className="lg:col-span-8 col-span-1 transition-all duration-500 ">
                    {posts.map((ele, ind) => (
                        <div key={ind + 1}>
                            <PostCard post={ele.node} />
                        </div>
                    ))}
                </div>

                <div className="col-span-1 lg:col-span-4 transition-all duration-500 ">
                    <div className="relative lg:sticky  top-8">
                        <PostWidget />
                        <Categories />
                    </div>
                </div>
            </div>
        </div>
    );
}

export const getStaticProps = async () => {
    const posts = (await getPosts()) || [];

    return {
        props: {
            posts,
        },
        revalidate: 10,
    };
};
