module.exports = {
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        fontFamily: {
            Inter_Black: [`Inter_Black`],
            Inter_Black_Italic: [`Inter_Black_Italic`],
            Inter_Bold: [`Inter_Bold`],
            Inter_Bold_Italic: [`Inter_Bold_Italic`],
            Inter_Extra_Bold: [`Inter_Extra_Bold`],
            Inter_Extra_Bold_Italic: [`Inter_Extra_Bold_Italic`],
            Inter_Extra_Light: [`Inter_Extra_Light`],
            Inter_Extra_Light_Italic: [`Inter_Extra_Light_Italic`],
            Inter_Italic: [`Inter_Italic`],
            Inter_Light: [`Inter_Light`],
            Inter_Light_Italic: [`Inter_Light_Italic`],
            Inter_Medium: [`Inter_Medium`],
            Inter_Inter_MediumItalic: [`Inter_Inter_MediumItalic`],
            Inter_Regular: [`Inter_Regular`],
            Inter_SemiBold: [`Inter_Inter_SemiBold`],
            Inter_Semi_Bold_Italic: [`Inter_Semi_Bold_Italic`],
            Inter_Thin: [`Inter_Thin`],
            Inter_Thin_Italic: [`Inter_Thin_Italic`],
            Roboto_Regular: [`Roboto_Regular`],
        },
        extend: {
            backgroundImage: {
                "nav-modal": `url('/nav-modal.svg')`,
                "lg-nav-modal": `url('/lg-nav.svg')`,
                welcome: `url('/Welcome.gif')`,
            },
        },
    },
    plugins: [],
};
