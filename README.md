# Hi, I'm Fardin Omor Afnan! 👋

# A Personal Blog Application

This is a personal blog application developed with next js

## Live Link

-   [Frontend](https://cms-blog-six-sooty.vercel.app/)
-   [Full Code](https://gitlab.com/fardinOA/blog-application/-/tree/main)

## Support

For support, email afnan.eu.cse@gmail.com
