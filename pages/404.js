import Link from "next/link";
import React from "react";

export default function Custom404() {
    return (
        <div className=" bg-white h-screen w-screen absolute top-0 flex justify-center items-center ">
            <div className="flex flex-col   justify-between h-[50%]">
                <p className="bg-red-300   p-4 text-[1.5rem]">
                    The url is not correct! please type carefully
                </p>
                <Link
                    className="bg-green-300 transition-all duration-300 hover:bg-green-400  text-center p-4 text-[1.5rem]"
                    href={`/`}
                >
                    Go to home
                </Link>
            </div>
        </div>
    );
}
