import React, { useEffect, useState } from "react";
import { getFeaturePost } from "../services";

import Link from "next/link";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import Image from "next/image";

import { Carousel } from "react-responsive-carousel";
import { motion } from "framer-motion";
import { dropInFromLeft } from "../animations/animations";
const FeaturedPost = () => {
    const [featuredPost, setFeaturedPost] = useState([]);

    useEffect(() => {
        getFeaturePost().then((res) => setFeaturedPost(res));
    }, []);

    return (
        <motion.div
            initial="hidden"
            animate="visible"
            exit="exit"
            variants={dropInFromLeft}
            className=" min-h-[100px] w-[300px]"
        >
            <h3 className="text-white font-bold text-4xl text-center mb-4">
                Featured Posts
            </h3>

            <Carousel
                swipeable={true}
                stopOnHover={true}
                infiniteLoop={true}
                autoPlay={true}
            >
                {featuredPost.map((post, ind) => (
                    <Link href={`/post/${post.slug}`} key={ind}>
                        <div className="  h-[50px] w-[50px] tranistion-all duration-200  ">
                            <Image
                                src={post.featuredImage.url}
                                alt={post.title}
                                width={50}
                                height={50}
                                loading="lazy"
                                layout="responsive"
                                className=" rounded-lg w-full h-full"
                            />

                            <p className="legend">
                                {post.title} By {post.author.name}
                            </p>
                        </div>
                    </Link>
                ))}
            </Carousel>
        </motion.div>
    );
};

export default FeaturedPost;
