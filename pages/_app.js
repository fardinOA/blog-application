import { DefaultSeo } from "next-seo";
import React from "react";

import Layout from "../components/Layout";
import "../styles/globals.css";
import SEO from "../nex-seo.config";
function MyApp({ Component, pageProps }) {
    return (
        <Layout>
            <DefaultSeo {...SEO} />
            <Component {...pageProps} />
        </Layout>
    );
}

export default MyApp;
